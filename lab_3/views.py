from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect, render
from lab_1.models import Friend 
from lab_3.forms import FriendForm

# Create your views here.
@login_required(login_url='/admin/login/')

def index(request):
    friends = Friend.objects.all().values()
    response = {'friends': friends}
    return render(request, 'lab3_index.html', response)
    
@login_required(login_url='/admin/login/')
def add_friend(request):
    context = {}
    form = FriendForm(request.POST or None, request.FILES or None)
      
    # check if form data is valid
    if form.is_valid():
        # save the form data to model
        form.save()
        return redirect('index')
  
    context['form']= form
    return render(request, "lab3_form.html", context)
