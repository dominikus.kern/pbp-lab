from django import forms
from lab_1.models import Friend


class FriendForm(forms.ModelForm):
    class Meta:
        model  = Friend
        fields = 'name','npm','dob'

        error_messages = {
            'required':'Please Type'
        }
        input_attrs = {
            'type':'text',
            'placeholder': 'Nama Anda''NPM Anda'
            
        }
        name = forms.CharField(label="",required = False, max_length = 30,widget= forms.TextInput(attrs = input_attrs))
        npm = forms.CharField(label="",required = False, max_length = 30,widget= forms.TextInput(attrs = input_attrs))
        dob = forms.DateField(label="",required = False)
        

