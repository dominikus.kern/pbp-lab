from django.conf.urls import url
from django.urls import path
from lab_2.views import index
from lab_2.views import xml
from lab_2.views import json

urlpatterns = [
    path('', index, name='index'),
    path('xml/',xml,name= 'xml'),
    path('json/',json, name = 'json')
    # TODO Add friends path using friend_list Views
]