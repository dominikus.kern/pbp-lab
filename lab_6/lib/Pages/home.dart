

import 'package:flutter/material.dart';
import 'package:lab_6/components/Navbar.dart';
import 'package:lab_6/components/categorySelector.dart';
import 'package:lab_6/components/navbar.dart';
import 'package:lab_6/components/newsCarousel.dart';
import 'package:lab_6/components/titledNewsView.dart';
import 'package:lab_6/components/topbar.dart';
import 'package:lab_6/components/welcomeWidget.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      body: SafeArea(
        child: Container(
            width: size.width,
            child: SingleChildScrollView(
              scrollDirection: Axis.vertical,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    padding: EdgeInsets.all(10),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        //NavBar(),
                        TopBar(),
                        SizedBox(height: 10),
                        //WelcomeWidget(),
                      ],
                    ),
                  ),
                  SizedBox(height: 5),
                  NewsCarousel(),
                  SizedBox(
                    height: 15,
                  ),
                  // CategorySelector(),
                  // SizedBox(
                  //   height: 5,
                  // ),
                  WelcomeWidget(),
                  Container(
                    padding: EdgeInsets.all(10),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [TiledNewsView()],
                    ),
                  )
                ],
              ),
            )),
      ),
    );
  }
}