import 'dart:io';

import 'package:flutter/material.dart';
// Future main() async {
//   WidgetsFlutterBinding.ensureInitialized();
  
//   var Firebase;
//   await Firebase.initializeApp();
  
// }

class BelajarForm extends StatefulWidget {
 
  @override
  _BelajarFormState createState() => _BelajarFormState();
}
class _BelajarFormState extends State<BelajarForm> {
  final _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("CaseWorqer", style: TextStyle(color: Colors.red[700], fontFamily: 'Sansita One')),
        backgroundColor: const Color(0xFF5CDB95),
        centerTitle: true,
        ),
      
      body: Form(
        key: _formKey,
        child: Container(
          padding: EdgeInsets.all(20.0),
          child: Column(
            children: [
             // tambahkan komponen seperti input field disini
             TextFormField(
                autofocus: true,
                decoration: new InputDecoration(
                    hintText: "Masukan Judul Artikel",
                    labelText: "Judul Artikel",
                    border: new OutlineInputBorder(
                      borderRadius: new BorderRadius.circular(15)
                    )
                    ),
                  validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Please enter some text';
                  }
                  return null;
                },
                
              ),
              new Padding(padding: EdgeInsets.only (top: 20.0)),
               TextField(
                keyboardType: TextInputType.multiline,
                maxLines: 2,
                autofocus: true,
                decoration: new InputDecoration(
                    hintText: "Masukan Ringkasan",
                    labelText: "Ringkasan",
                    border: new OutlineInputBorder(
                      borderRadius: new BorderRadius.circular(15)
                    )
                    ),
                // validator: (value) {
                //   if (value == null || value.isEmpty) {
                //     return 'Please enter some text';
                //   }
                //   return null;
                // },
                
              ),
              new Padding(padding: EdgeInsets.only (top: 20.0)),
              TextField(
                keyboardType: TextInputType.multiline,
                maxLines: 5,
                autofocus: true,
                decoration: new InputDecoration(
                    hintText: "Masukan Artikel",
                    labelText: "Artikel",
                    border: new OutlineInputBorder(
                      borderRadius: new BorderRadius.circular(15)
                    )
                    ),
                // validator: (value) {
                //   if (value == null || value.isEmpty) {
                //     return 'Please enter some text';
                //   }
                //   return null;
                // },
                
              ),
              
              ElevatedButton(
                  onPressed: () {
                    // Validate will return true if the form is valid, or false if
                    // the form is invalid.
                    if (_formKey.currentState!.validate()) {
                      // Process data.
                    }
                  },
                  child: const Text('Submit'),
                ),
          

            ],
          ),
        ),
      ),
    );
  }
}