from django.conf.urls import url
from django.urls import path
from lab_4.views import index
from lab_4.views import add_note
from lab_4.views import note_list


urlpatterns = [
    path('', index, name='index'),
    path('add-note/', add_note, name='add_note'),
    path('note-list/', note_list, name='note_list'),
    # TODO Add friends path using friend_list Views
]