from django.shortcuts import render,redirect
from lab_2.models import Note
from lab_4.forms import NoteForm
# Create your views here.
def index(request):
    notes = Note.objects.all().values()
    response = {'notes' : notes}
    return render(request, "lab4_index.html", response)
def add_note(request):
    context = {}
    form = NoteForm(request.POST or None, request.FILES or None)
        
    # check if form data is valid
    if form.is_valid():
        # save the form data to model
        form.save()
        return redirect('http://127.0.0.1:8000/lab-4/note-list/')
    context['form']= form
    return render(request, "lab4_form.html", context)  

def note_list(request):
    notes = Note.objects.all().values()
    response = {'notes' : notes}
    return render(request, "lab4_note_list.html", response)