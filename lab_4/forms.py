from django import forms
from lab_2.models import Note


class NoteForm(forms.ModelForm):
    class Meta:
        model  = Note
        fields = 'To','From','Title','message'

        error_messages = {
            'required':'Please Type'
        }
        input_attrs = {
            'type':'text',
            'placeholder': 'Nama Penerima''Nama Pengirim''Judul''Masukkan Pesan'
            
        }
        To = forms.CharField(label="",required = False,widget= forms.TextInput(attrs = input_attrs))
        From = forms.CharField(label="",required = False,widget= forms.TextInput(attrs = input_attrs))
        Title = forms.CharField(label="",required = False, widget= forms.TextInput(attrs = input_attrs))
        message = forms.CharField(label="",required = False, widget= forms.TextInput(attrs = input_attrs))
    
        

