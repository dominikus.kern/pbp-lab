Dominikus Kern Bunardi

2006464423

1. Apakah perbedaan antara JSON dan XML?

JSON merupakan singkatan dari JavaScript Object Notation.  JSON adalah turunan JavaScript yang digunakan dalam transfer dan penyimpanan data. 
JSON memiliki format untuk pertukaran data. Tipe data Json yaitu data skalar, struktur data dapat diekspresikan menggunakan array dan objek. 
JSON memungkinkan string apa pun sebagai pengidentifikasi, mendukung array , dan UTF-8 tetapi tidak mendukung namespace dan komentar. 
JSON tidak menggunakan tag akhir. JSON digunakan untuk representasi objek sehingga kurang aman.

XML merupakan singkatan dari eXtensible Markup Language. 
XML adalah bahasa markup yang mendefinisikan serangkaian  aturan untuk dalam membuat dokumen dengan format yang dapat dibaca manusia dan mesin. 
Tipe data XML bergantung pada jenis informasi yang ditambahkan sehingga xml lebih kompleks dan memiliki tools yang lebih banyak. 
XML mendukung namespace dan komentar tetapi tidak mendukung array. XML menggunakan tag pada  awal dan akhir. 
XML berfungsi sebagai bahasa markup dan menggunakan struktur untuk representasi item data. Dengan demikian, XML menjadi lebih aman.

2.	Apakah perbedaan antara HTML dan XML?
HTML merupakan singkatan dari Hypertext Markup Language. HTML adalah bahasa markup standar yang digunakan untuk membuat halaman web atau aplikasi web.  
HTML tidak mengandung informasi struktural. HTML bersifat tidak peka dengan hutuf besar atau kecil. Error kecil pada HTML dapat diabaikan. 
Tag penutup html bersifat optional. HTML berfokus pada penyajian informasi halaman web dan memiliki fungsi untuk pengembangan struktur halaman web.

XML merupakan singkatan dari Extensible Markup Language . 
XML adala bahasa markup yang mendefinisikan serangkaian  aturan untuk dalam membuat dokumen dengan format yang dapat dibaca manusia dan mesin. 
XML menyediakan sebuah framework sehingga kita dapat menentukan bahasa markup yang akan digunakan. XML bersifat case sensitive.
Error pada XML tidak dapat diabaikan dan harus menggunakan tag penutup. 
XML berfokus dalam perpindahan informasi dan memilki kegunaan untuk pertukaran data anatar platform

